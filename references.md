# Useful links to learn more

_This list cannot be considered **by no means** complete. It includes a few links related to some topics discussed more or less in-depth during the workshops._


- [PEP 8 document](https://www.python.org/dev/peps/pep-0008/)
- [Python f-strings](https://realpython.com/python-f-strings/)
- [How to obtain binary representations of floats](https://stackoverflow.com/a/16444778/5701517)
- [More ways of rounding in Python](https://realpython.com/python-rounding/)
- [Differences between 'math.pow' and built-in 'pow'](https://stackoverflow.com/a/10282852/5701517)
- [Formatting language of f-string](https://docs.python.org/3/library/string.html#format-specification-mini-language)
- [When to use inner functions](https://realpython.com/inner-functions-what-are-they-good-for/)
- [Built-in exceptions](https://docs.python.org/3/library/exceptions.html)
- [Conditional statements](https://realpython.com/python-conditional-statements/)
- [Shallow vs deep copying](https://realpython.com/copying-python-objects/)


Rembemer that Python has an extremely [complete documentation](https://docs.python.org) which should be your first search for a language's detail.

If instead you are looking for a solution to a problem, you should look at one of the numerous developers community and forums. [StackOverflow.org](https://stackoverflow.com/questions/tagged/python) is a (very) good starting point.


# Access and run the Jupyter notebooks

The coding of day 2 and 3 has been carried out in Jupyter notebooks. If you don't know what they are, for now it's enough to say that they provide an **interactive Python environment** with many more features than a simple Python console. You can go to [Jupyter.org](https://jupyter.org/) and learn more!

If you want to execute some of the code we went through during the workshop, you can access the notebooks on [Binder.org](https://mybinder.org/v2/gl/ebaldi%2Fpython-intro-workshops/c447209d937f56f2b432c4ade3e04a70a58ca2e2).

### Recordings

- [Day 1](https://us02web.zoom.us/rec/share/osOtLFialvJWjMuGnfTeXaUGfsQC9p0HVKhtnYkefHcOZs24ayC4e_Py6no0A0A.eexmQjnSQU7w4-3s) (passcode: `RB0.1xqS`)
- [Day 2](https://us02web.zoom.us/rec/share/WO4_Bf6N-rCFHPP3x9ohUCs1xjbdNn1CLcbDJgrBA9HtQ-daDjrlv0KTT_x8jFZW.GZCx_xgus_dy2GYj) (passcode: `4qJv0+%Q`)
- [Day 3](https://us02web.zoom.us/rec/share/K-C9IrTHLaUbqAAWGdPDyxrYWZ0Xj6x_TvHnaiQE8mXN9anGVcfAbZg5BCt7YSZ3.shiWe66RwBMybyqi) (passcode: `%i3Mk9pe`)

# Today's topics
# 0. Why Python
# 1. Variables
#   - Types
#      - Strings
#      - Integers
#      - Floats
#      - Booleans
# 2. Functions, loops, control flow
# 3. Python data structures (Tomorrow)

age = 35
your_age = 42 # A-Z, a-z, 0-9, _

name = "Edoardo"
name_2 = 'Edoardo'
text = "Hello, my name is 'Edoardo'"

multi_line_text = """Hello,
My name is 'Edoardo'!
And yours?"""

surname = "Baldi"
full_name = name + " " + surname

name[0] # First char
name[-5] # Fifth before the last

name[0:3] # [i:j] includes i BUT NOT j

wrong_name = "Wdoardo"

# f-string
text = f"My name is {name} and my age is {age}"
text = "My name is {} and my age is {}".format(name, age)

# .find(search), .replace(old, new)

# Exercise
"""
Write a program that asks for user input and convert the entered text making the following changes
    - a → 4
    - b → 8
    - e → 3
    - l → 1
    - o → 0
    - s → 5
    - t → 7

You should deal with mixed-case input: HELLO, this is a StRiNG!
"""

text = str(input("Enter text: "))
new_text = text.lower()

new_text = new_text.replace('a', '4')
new_text = new_text.replace('b', '8')
new_text = new_text.replace('e', '3')
new_text = new_text.replace('l', '1')
new_text = new_text.replace('o', '0')
new_text = new_text.replace('s', '5')
new_text = new_text.replace('t', '7')
print(text)
print(new_text)

# Two types of number: integral, rationals, floats

# 2/3, 1/3, 1/2, 22/7

# Floating point representation error
# 0.1 + 0.2 = 0.3
# (0.1)_10 = (0.0 0011 0011 0011 0011 ...)_2
#
# Binary repr of 0.1 + 0.2 /= binary repr of 0.3

# Printing float
import math
text = "Rounding PI to 4 digits {:.4f}"
text.format(math.pi)

# Percentages
prob = 0.78
text = f"Probability of winning = {prob:.1%}"

# Booleans
# False -> 0
# True -> 1

# For numbers, bool(x) will return True if x /= 0, otherwise False
# bool(obj) returns the "truthyness" of the object

# Logical operators
# ()
# ==, !=, >, >=, <, <=
# not
# and
# or

# X and Y -> True only if both are True, otherwise False
# If X is "falsy", then return X. Otherwise, EVALUATE and return Y

# X or Y -> False only if both are False, otherwise True
# If X is "truthy", then return X. Otherwise, EVALUATE and return Y

# Quick exercise: write a statemente tha returns the first char of string if is NOT empty or None. Otherwise, return the empty string ''

# SOLUTION: s and s[0] or ''

# EXERCISE
# - We want to check if a 'name' retrieved from a database starts with a digit (0-9)
# - If the variable is not present in db, Python receives a 'null' object which is translated to 'None'
# 
# How can you write such check?



# There are TWO more logical operators: 'is', 'in'
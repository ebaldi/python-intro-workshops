# This exercise required reading from input the file "ints.txt"
# Use this program as follows:
#
#   python 3-find_the_sum.py < ints.txt
#
# Make sure that 'ints.txt' is in the same folder, or use the correct path

import fileinput

ints = [int(line) for line in fileinput.input()]

num_ints = len(ints)

for i in range(num_ints):
    for j in range(i + 1, num_ints):
        if ints[i] + ints[j] == 2020:
            print(f"1. The product of {ints[i]} x {ints[j]} is {ints[i] * ints[j]}")
        for k in range(j + 1, num_ints):
            if ints[i] + ints[j] + ints[k] == 2020:
                print(f"2. The product of {ints[i]} x {ints[j]} x {ints[k]} is {ints[i] * ints[j] * ints[k]}")


# Odd or even?

Ask the user for a number. Depending on whether the number is even or odd, print out an appropriate message to the user.

### Extras

1. If the number is a multiple of 4, print out a different message.

2. Ask the user for two numbers: one number to check (call it `num`) and one number to divide by (`check`). If `check` divides evenly into `num`, tell that to the user. If not, print a different appropriate message.

#### Hints

We are going to see today (17 June) the full syntax of an `if` block. But here's an example to give you an help with the exercise:
```
if test-condition:
    print("Condition is True")
elif another-condition:
    print("Another condition is True")
else:
    print("All the conditions are False")
```

Remember:

- **Indentation is important** in Python. You will get an error if you forget it or type it incorrectly.

- By default, indentation is **4 blank spaces**. But you can choose 2, 3, or 6, as long as **you are consistent**.
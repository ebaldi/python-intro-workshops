# Solutions to exercises

To run a solution file on a system with Python installed (Windows, macOS, Linux), use ```python <file_name>.py```

On macOS you might need to run `python3` explicitly because the system Python is usually 2.7.x.

If you need install the latest Python for your Operating System, just go to to [python.org](https://www.python.org/downloads/).

## Day 1

- `ex_1.py`: When will I be 100 years old?
- `ex_2.py`: Odd or even?
- `ex_3.py`: Track your investments' gain

## Day 2

- `ex_5.py`: Change the temperature

## Day 3

- `ex_6.py`: Guess the capital city
- `ex_7.py`: Anagrams
    - `ex_7_primes_.py` is the solution with prime numbers
import random

# generate a list of lists with random integers in the interval (-10, 10)
# dimensions: 10 x 3
items = [
    [random.randint(-10,10) for _ in range(3)]
    for _ in range(10)
    ]

# we need a function that returns the indexes of a sorted list
def argsort(_list):
    return [
        x[0] for x in sorted(enumerate(_list), key=lambda x: x[1])
    ]

# print the input list before sorting
print(f"This is the input list:\n{items}")

for i, x in enumerate(items):
    if i == 0: # if it's the first element, get the sorted indexes
        order = argsort(x)
        print(f"Sorting order will be: {order}")
        x.sort() # then sort the first sub-list
    else:
        # sort all the other sublists with the given order
        items[i] = [x[j] for j in order]

# print the list after sorting
print(f"This is the sorted list:\n{items}")
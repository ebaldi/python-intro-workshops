from en_dict import words

def are_anagrams(word_1, word_2):
    """
        1. Convert input words to lowercase, remove spaces
        2. Create two *sorted* lists with the characters of each word: if the two lists are equal, the words are anagrams
    """
    words = [sorted(word.lower().replace(' ' , '')) for word in word_1, word_2]
    return words[0] == words[1]

def run():
    word_1 = input("Insert the first word: ")
    word_2 = input("Insert the second word: ")

    # Part 1: check if word_1 and word_2 are anagrams
    if word_2: # only if word_2 was supplied by the user
        if are_anagrams(word_1, word_2):
            print(f"{word_1} is an anagram of {word_2}")
        else:
            print(f"Words '{word_1}, {word_2}' aren't anagrams")

    # Part 2: return all the anagram words from the `en_dict.txt` list of words
    anagrams = []
    for word in words:
        if are_anagrams(word_1, word):
            anagrams.append(word)
    anagrams.remove(word_1.lower())
    print(f"Found {len(anagrams)} anagrams of '{word_1.lower()}':\n{anagrams}")

if __name__ == "__main__":
    run()

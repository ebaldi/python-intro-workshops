import random
from cities import capitals_dict

def run():
        state, capital = random.choice(list(capitals_dict.items()))

        while True:
            guess = input(f"What's the capital of '{state}'? ")
            guess = guess.lower()
            if guess == 'exit':
                print(f"The capital of '{state}' is '{capital}'.")
                print('Goodbye')
                break
            elif guess == capital.lower():
                print('Correct answer!')
                break

if __name__ == "__main__":
    run()
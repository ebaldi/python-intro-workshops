def run():
    def convert_cel_to_far(temp_cel):
        """Return the Celsius temperature temp_cel converted to Fahrenheit"""
        return temp_cel * (9 / 5) + 32

    def convert_far_to_cel(temp_far):
        """Return the Fahrenheit temperature temp_far converted to Celsius"""
        return (temp_far - 32) * (5 / 9)

    temp_far = float(input('Enter a temperature in degrees F: '))

    temp_cel = convert_far_to_cel(temp_far)

    print(f"{temp_far} degrees F = {temp_cel:.2f} degrees C")

    temp_cel = float(input('Enter a temperature in degrees C: '))

    temp_far = convert_cel_to_far(temp_cel)

    print(f"{temp_cel} degrees C = {temp_far:.2f} degrees F")

if __name__ == "__main__":
    run()
def run():
    name = input("What is your name: ")
    try:
      age = int(input("How old are you: "))
    except ValueError:
      print("Age must be an integer!")
      raise

    year = str((2021 - age) + 100)

    print(f"{name}, you will be 100 years old in the year {year}")

if __name__ == "__main__":
    run()

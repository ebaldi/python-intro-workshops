def run():
    amount = float(input("Enter a principal amount: "))
    rate = float(input("Enter an annual rate of return: "))
    years = int(input("Enter a number of years: "))

    for year in range(1, years + 1):
        amount = amount * (1 + rate)
        print(f"year {year}: ${amount:,.2f}")

if __name__ == "__main__":
    run()
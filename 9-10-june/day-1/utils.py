import ctypes
import sys


def ref_count(address):
    return ctypes.c_long.from_address(address).value


def sys_ref_count(var):
    return sys.getrefcount(var)

"""
In this exercise, you’ll write a program that generates poetry.
- Create five lists for different word type
    - Nouns: `["fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert", "gorilla"]`
    - Verbs: `["kicks", "jingles", "explodes", "curdles"]`
    - Adjectives: `["furry", "balding", "exuberant", "glistening"]`
    - Prepositions: `["against", "after", "for", "in", "like", "over", "within"]`
    - Adverbs: `["curiously", "furiously", "sensuously"]`
- Randomly select the following number between each list
    - 3 nouns
    - 3 verbs
    - 3 adjectives
    - 2 prepositions
    - 1 adverb
- Use the `.choice(iterable)` method of the `random` module
- Print out the poem with the following format

  {A/An} {adj1} {noun1}

  {A/An} {adj1} {noun1} {verb1} {prep1} the {adj2} {noun2}
  {adverb1}, the {noun1} {verb2}
  the {noun2} {verb3} {prep2} a {adj3} {noun3}
"""


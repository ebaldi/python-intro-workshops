"""
Write a function called `factors.py` that asks the user to input a positive integer and then prints out the factors of that number. Here’s a sample run of the program with output:

Enter a positive integer: 12
1 is a factor of 12
2 is a factor of 12
3 is a factor of 12
4 is a factor of 12
6 is a factor of 12
12 is a factor of 12
"""

num = int(input("Please, enter an integer: "))
for divisor in range(1, num + 1):
  if num % divisor == 0:
    print(f"{divisor} is a factor of {num}")

def invest(amount, rate, years):
  for year in range(1, years + 1):
    amount *= (1 + rate)
    print(f"Year {year}: {amount:.2f}")

amount = float(input("Enter your starting amount: "))
rate = float(input("Enter the annual rate: "))
years = int(input("Enter a number of year: "))

invest(amount, rate, years)
"""
- Write a function that calculates the growing amount of an investment over time. An initial deposit, called the principal amount, is made. Each year, the amount increases by a ﬁxed percentage, called the annual rate of return.

- Write a function called invest with three parameters: the principal amount, the annual rate of return, and the number of years to calculate. The function signature might look something like this: `def invest(amount, rate, years):`

- The function then prints out the amount of the investment, rounded to 2 decimal places, at the end of each year for the speciﬁed number of years.

- To ﬁnish the program, prompt the user to enter an initial amount, an annual percentage rate, and a number of years. Then call invest() to display the calculations for the values entered by the user.
"""
"""
Write a script that receives two numbers from the user and displays the ﬁrst number raised to the power of the second number.

Sample output expected:

Enter a base: 1.2
Enter an exponent: 3
1.2 to the power of 3 = 1.7279999999999998
"""
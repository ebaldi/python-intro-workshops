### Recordings

- Day 1: [morning](https://us02web.zoom.us/rec/share/0qQ0ZcRuUE15rUWOnZ-BhMEoK72ri5a64Q1t8p3pF3Gf3vFT9aYrfLN4278LQooz.c3HZ0-4R3T__POb6) (passcode: `QPAx$K7Y`), [afternoon](https://us02web.zoom.us/rec/share/CQFLJ3oEaUmX6KCkpr9_IP8MilOdbeFAXwglD7vHgXhfcqyw7de4KoQOd243yNg.y-iSdUeU--_RvVF6) (passcode: `hJ.bjb$4`)

- Day 2: [morning](https://us02web.zoom.us/rec/share/4QfUKjmeNGrHhq3Ts0AKsStxvp8swpdFaetwRDc8_JwOWPmZ0ivMUXIz_j5_WFF7.5U3kgibsL4g28nXS) (passcode: `^2Nm5Cm=`), [afternoon](https://us02web.zoom.us/rec/share/6NIyknDUi5pKMTCAnCtOKRD9Lod82h6dV54PcMFFqYem3LyXFODDpd0m9b2tk6Q.bR4mBU2wNeSJslcM) (passcode: `u?tiz%?7`)

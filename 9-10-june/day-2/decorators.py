# Decorators

# 1. Functions are objects: can be assigned to names and passed to or returned from other functions
# 2. Functioncs can be nested and some inner functions can remember the state of the local scope ("closures")

def null_decorator(func):
    return func


#greet = null_decorator(greet) # wrapping our function 'greet' with 'null_decorator'


def uppercase(func):
    def wrapper():
        original_result = func()
        modified_result = original_result.upper()
        return modified_result

    return wrapper


#@uppercase
#def greet():
#  return 'Hello!'

#print(greet())


def bold(func):
    def wrapper():
        return '<strong>' + func() + '</strong>'

    return wrapper


def emph(func):
    def wrapper():
        return '<em>' + func() + '</em>'

    return wrapper


@bold
@emph
def greet():
  return 'Hello!'

#print(greet())

# Decorating a function with arguments
def proxy(func):
  def wrapper(*args, **kwargs):
    return func(*args, **kwargs)
  return wrapper

# More useful example: a simple logger
def logger(func):
  def wrapper(*args, **kwargs):
    print(
      f'LOG: calling function {func.__name__}() '
      f'with {args}, {kwargs}'
    )

    original_result = func(*args, **kwargs)

    print(
      f'LOG: {func.__name__}() returned {original_result}'
    )
    return original_result
  return wrapper

@logger
def speak(name, line, mode = 'whisper'):
  return f'{name}: {line}'

print(speak('Edoardo', "I'm a decorated line!", mode = 'SHOUT'))

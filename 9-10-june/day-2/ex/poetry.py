import random

noun = [
    "fossil", "horse", "aardvark", "judge", "chef", "mango", "extrovert",
    "gorilla"
]

verb = ["kicks", "jingles", "explodes", "curdles"]

adjective = ["kicks", "jingles", "explodes", "curdles"]

preposition = ["against", "after", "for", "in", "like", "over", "within"]

adverb = ["curiously", "furiously", "sensuously"]

def make_poem():
    """Create a random poem from a list of words"""

    # Pick 3 random nouns
    n1 = random.choice(noun)
    n2 = random.choice(noun)
    n3 = random.choice(noun)

    while n1 == n2:
        n2 = random.choice(noun)
    while n1 == n3 or n2 == n3:
        n3 = random.choice(noun)

    # Pick 3 random verbs
    v1 = random.choice(verb)
    v2 = random.choice(verb)
    v3 = random.choice(verb)

    while v1 == v2:
        v1 = random.choice(verb)
    while v1 == v3 or v2 == v3:
        v3 = random.choice(verb)

    # Pick 3 random adjectives
    a1 = random.choice(adjective)
    a2 = random.choice(adjective)
    a3 = random.choice(adjective)

    while a1 == a2:
        a2 = random.choice(adjective)
    while a1 == a3 or a2 == a3:
        a3 = random.choice(adjective)

    # Two different prepositions
    prep1 = random.choice(preposition)
    prep2 = random.choice(preposition)

    while prep1 == prep2:
        prep2 = random.choice(preposition)

    # One adverd
    adv1 = random.choice(adverb)

    # Pick the correct article
    vowels = "aeiou"
    if vowels.find(a1[0]) != -1:
        article = "An"
    else:
        article = "A"

    poem = f"""
    {article} {a1} {n1}

    {article} {a1} {n1} {v1} {prep1} the {a2} {n2}
    {adv1}, the {n1} {v2}
    the {n2} {v3} {prep2} a {a3} {n3}
    """

    return poem
